package troop.generator.generator.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.StopWatch;
import troop.generator.generator.port.in.GenerateTroops;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
public class GenerateTroopsImplTest {


    private static final double[] amountPool = {10000,100000,500000,1000000,5000000,
    10000000,100000000,1000000000,10000000000L};

    private static final double[] kindsPool = {3,10,20,50,100,1000,5000,10000};



    @Test
    @DisplayName("Test that the response amounts sum up to the requested one")
    public void testTotalSumOfResult() {

    GenerateTroops generateTroops = new GenerateTroopsImpl();

    //loop through different amounts of troops and different amounts of kinds
        for (double amount : amountPool) {
            for (double kinds : kindsPool) {
                List<BigInteger> result = generateTroops.generateTroops(amount, kinds);
                assertThat(Arrays.stream(result.stream().mapToDouble(BigInteger::doubleValue).toArray()).sum(),
                        is(equalTo(amount)));
                assertThat(result.size(),is(equalTo((int)kinds)));
                assertThat(result.stream().filter(m->m.doubleValue()==0).collect(Collectors.toList()).size(),is(equalTo(0)));
            }
        }
}

// This test assures that with a 96% confidence when asking for 25 times for 167 troops of 3 kinds
// the returned results will all differ
    @Test
    @DisplayName("Test that the response amounts are random")
     public void randomnessOfResult(){

        final int numberOfTests=100000;
        final int numOfTries = 25;
        final long numOfTriesLong = 25L;
        GenerateTroops generateTroops = new GenerateTroopsImpl();
        double [] hashValues = new double[numOfTries];
        double [] normalizedValues;
        double succeeded=0;


        for (int j=0; j<numberOfTests; j++) {
            for (int i = 0; i < numOfTries; i++) {
                List<BigInteger> result = generateTroops.generateTroops(167, 3);
                normalizedValues = result.stream().mapToDouble(BigInteger::doubleValue).toArray();
                hashValues[i] = normalizedValues[0] + normalizedValues[1] * 167 + normalizedValues[2] * 27889;
            }
            if(Arrays.stream(hashValues).distinct().count()==numOfTriesLong){
                succeeded++;
            }
        }
        assertThat(succeeded/numberOfTests>0.96,is(equalTo(true)));

        }


    @Test
    @DisplayName("Test that the time complexity is O(1)")
    public void timeComplexity(){
        GenerateTroops generateTroops = new GenerateTroopsImpl();

        //loop through different amounts of troops and different amounts of kinds
        for (double amount : amountPool) {
            for (double kinds : kindsPool) {
                StopWatch watch = new StopWatch();
                watch.start();
                List<BigInteger> result = generateTroops.generateTroops(amount, kinds);
                watch.stop();
                assertThat(watch.getTotalTimeMillis() < 10, is(equalTo(true)));
            }
        }
    }

}
