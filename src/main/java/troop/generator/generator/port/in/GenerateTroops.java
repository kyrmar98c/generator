package troop.generator.generator.port.in;


import java.math.BigInteger;
import java.util.List;


public interface GenerateTroops {

     List<BigInteger> generateTroops(double total, double kinds );

}
