package troop.generator.generator.rest.in;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import troop.generator.generator.port.in.GenerateTroops;

import java.math.BigInteger;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
public class GenerationApplicationController {

    private GenerateTroops generateTroops;

    @GetMapping("/generate/troops/{amount}/{kinds}")
    ResponseEntity<List<BigInteger>> getGenerateTroops(@PathVariable double amount, @PathVariable double kinds){
        StopWatch watch = new StopWatch();
        watch.start();
        List<BigInteger> generatedTroops = generateTroops.generateTroops(amount,kinds);
        watch.stop();
        log.info("Total execution time to create " +amount +" troops of " +kinds + " different types in millis: "
                        + watch.getTotalTimeMillis());
        return ResponseEntity.ok().body(generatedTroops);
    }

}
