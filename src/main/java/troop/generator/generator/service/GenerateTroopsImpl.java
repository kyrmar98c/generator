package troop.generator.generator.service;

import org.springframework.stereotype.Service;
import troop.generator.generator.port.in.GenerateTroops;


import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


@Service
public class GenerateTroopsImpl implements GenerateTroops {


    @Override
    public List<BigInteger> generateTroops(double total, double kinds) {


        double totalWithOneReserved = total - kinds;

        Random random = new Random();

        // generate random numbers

        double[] doubles = new double[(int)kinds];

        doubles = Arrays.stream(doubles).map(m-> Math.random()).toArray();

       // long[] doubles = Math.random();

        //take their sum

        double totalSum =  Arrays.stream(doubles).sum();

        // divide each number with sum multiply with total and floor
        // Add 0.5 before floor
        // by the law of big numbers this will tend to be 0 as kinds is greater
        // thus the while loop is of time complexity O(1)
       doubles = Arrays.stream(doubles).map(
                m -> Math.floor(m*totalWithOneReserved/totalSum + 0.5)
        ).toArray();


        // find how much more needs to be added to reach total
        double remain = totalWithOneReserved - (Arrays.stream(doubles).sum());
        int kindsInt = (int) kinds;
        // step to update remain
        int step=0;

        //  if remain positive then reduce remain and add to doubles
        if(remain>0){
            step=-1;
        }
        // if remain negative then increase remain and remove from doubles
        else if(remain<0 ){
            step=1;
        }
        int index=0;
        // if remain is 0 no need to initiate this loop
        while ( remain!=0.0){
            index=random.nextInt(kindsInt);
            // don't subtract from 0
            if(doubles[index]==0 && step==1){
                continue;
            }
            doubles[index]-=step;
            remain+=step;
        }

        doubles=Arrays.stream(doubles).map(m->m+1).toArray();

       return  Arrays.stream(doubles).mapToObj(m-> BigInteger.valueOf((long)m)).collect(Collectors.toList());

    }
}
