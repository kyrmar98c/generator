FROM amazoncorretto:11-alpine-jdk
MAINTAINER baeldung.com
COPY target/generator-0.0.1-SNAPSHOT.jar generator.jar
ENTRYPOINT ["java","-jar","/generator.jar"]
